import * as readline from 'readline-sync';

class Calculator {
    add(inputArray : number[]) {
        let result : number = 0;

        inputArray.forEach(function (value : number) {
            result += value;
        });

        return result;
    }

    subtract(inputArray : number[]){
        let result : number = inputArray[0];
        let count : number = inputArray.length;

        for (let i = 1; i < count; i++) {
            result -= inputArray[i];
        }

        return result;
    }

    multiply(inputArray : number[]){
        let result : number = 1;

        inputArray.forEach(function (value) {
            result *= value;
        });

        return result;
    }

    divide(inputArray : number[]){
        let result : number = inputArray[0];
        let count : number = inputArray.length;

        for (let i = 1; i < count; i++) {
            result /= inputArray[i];
        }

        return result;
    }
}

let result : number;
let valueArray : number[] = [];
let calculator = new Calculator();

let operator : number = Number(readline.question("1.+  2.-  3.*  4./  (번호선택) : "));
let number : number = Number(readline.question("몇 개 숫자 입력? "));

for(let i = 0; i < number; i++){
    valueArray.push(Number(readline.question((i + 1) + " 번째 입력 : ")));
}

switch (operator) {
    case 1:
        result = calculator.add(valueArray);
        console.log(result);
        break;
    case 2:
        result = calculator.subtract(valueArray);
        console.log(result);
        break;
    case 3:
        result = calculator.multiply(valueArray);
        console.log(result);
        break;
    case 4:
        result = calculator.divide(valueArray);
        console.log(result);
        break;
    default :
        console.log("error");
        break;
}